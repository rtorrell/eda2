package pr2;

import java.util.*;

public interface AuthorshipTracker {
	
	public int addBooksToAuthor (Author author, Collection<Book> books);
	/* Binds the given author with the books in the second argument.
	   Books already bound to the author are not bound again.
	   Returns the number of books effectively bound to the author.
	 */
	
	public int addAuthorsToBook (Collection<Author> authors, Book book);
	/* Binds each author in the first argument with the given book.
	   if an author is already bound to the book, no binding occurs
	   for that author.
	   Authors may or may not be previously known.
	   Returns the number of authors affectively affected by the 
	   operation
	 */
	
	public SortedSet<Author> findAuthors (BookTag tag);
	/* Returns all the authors of the given book. 
	   The result is sorted by author's name (ascending)
	   if the book is unknown the result is an empty set	
	 */
	
	public SortedSet<Book> findBooks (String authorName);
	/* Returns all the books of the given author.
	   The result is sorted by title of the book (ascending)
	   if the author is unknown the result is an empty set
	 */
	
	public SortedSet<Book> findBooksInCommon (String name1, String name2);
	/*  Returns all the books that the arguments (authors) have
	    in common. The result is sorted according to the natural ordering
	    of the books.
	    If any of the authors is unknown the result is an empty set
	 */
}
