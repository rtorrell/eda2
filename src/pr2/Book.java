package pr2;


public class Book implements Comparable {

	private BookTag tag;
	private int year;
	private String title;
	private int copies;
	
	public Book (BookTag tag, int year, String title) {
		this.tag = tag;
		this.year = year;
		this.title = title.toUpperCase();
		this.copies = 1;
	}
	
	
	public BookTag getTag () {return this.tag;}
	public int getYear() {return this.year;}
	public String getTitle() {return this.title;}
	public int getCopies() {return this.copies;}
	
	public int addCopy () {
		this.copies++; 
		return this.copies;
	}
	
	public int removeCopy() {
		this.copies--;
		if (copies<0) throw new IllegalStateException("book with negative copies!!! "+this);
		return this.copies;
	}
	
	@Override
	public int compareTo(Object arg) {
	/* Books are sorted by booktag. Books with lowest booktags go first */
	
		Book argB = ((Book)arg);
		
		return this.getTag().compareTo(argB.getTag());
		
	}
	
	public boolean equals(Object arg) {
		try {
			return this.compareTo(arg)==0;
		}
		catch (ClassCastException cce) {
			return false;
		}
	}
	
	public String toString () {
		return tag+"-("+year+")-"+title;
	}
	
	public int hashCode () {
		return tag.hashCode();
	}
	
	public Book clone () {
		return new Book(tag.clone(), year, new String(title));
	}
	
}
