package pr2;


public class BookTag implements Comparable{
	
	private String left;
	private int mid;
	private String right;
	
	public BookTag (String left, int mid, String right) {
		check(left, mid, right);
		this.left = left.toUpperCase();
		this.mid = mid;
		this.right = right.toUpperCase();
	}
	
	
	@Override
	public int compareTo(Object arg) {
		/* Booktags are sorted as follows:
	 	- first go booktags with lowest left attribute. If left attributes cannot discriminate...
	 	- ... first go booktags with the lowest mid attribute. If mid cannot discriminate...
	 	- ... first go booktags with HIGHEST right attribute. 
		 */
		int actualReturn;
		BookTag argBT = ((BookTag)arg);
		actualReturn = Integer.parseInt(this.left) - Integer.parseInt(argBT.left);
		if(actualReturn == 0) {
			actualReturn = this.mid - argBT.mid;
			if(actualReturn == 0) {
				actualReturn = Integer.parseInt(this.right) - Integer.parseInt(argBT.right);
			}
		}
		return actualReturn;
		
	}
	
	@Override
	public boolean equals (Object arg) {
		try {
			return this.compareTo(arg)==0;
		}
		catch(ClassCastException cce) {
			return false;
		}
	}
	
	public String toString () {
		return "["+left+"-"+mid+"-"+right+"]";
	}
	
	
	public int hashCode () {
		return left.hashCode()%mid+right.hashCode()%mid;
	}
	
	private static void check (String left, int mid, String right) {
		if (left.length()!=4) throw new IllegalArgumentException("Bad left size: "+left);
		if (mid<10 || mid>99) throw new IllegalArgumentException("Bad mid number: "+mid);
		if (right.length()!=2) throw new IllegalArgumentException("Bad right size: "+left);
	}
	
	public BookTag clone() {
		return new BookTag(new String(left), mid, new String(right));
	}

}
