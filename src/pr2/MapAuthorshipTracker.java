package pr2;

import java.util.*;

public class MapAuthorshipTracker implements AuthorshipTracker {

	/* DO NOT MODIFY THIS */
	private Map<Author, Set<Book>> theMap;
	/* DO NOT ADD MORE ATTRIBUTES */
	
	public MapAuthorshipTracker () {
		theMap = new HashMap<Author, Set<Book>>();
	}
	
	/* COMPLETE */
	
	
	/* Complimentary method */
	private Author FindAuthorByName (String name) {
		for (Author a : theMap.keySet())
			if (name.toUpperCase().equals(a.getName().toUpperCase()))
				return a;
		return null;
	}

	@Override
	public int addBooksToAuthor(Author author, Collection<Book> books) {
		
		Set<Book> setToMap = new TreeSet<Book>();
		
		setToMap.addAll(books);
		theMap.put(author,setToMap);
		return theMap.put(author,setToMap).size();
	}

	@Override
	public int addAuthorsToBook(Collection<Author> authors, Book book) {
		
		Iterator<Author> authorIterator = authors.iterator();
		int addedAuthors = 0, actualAuthorSize;
		
		while(authorIterator.hasNext()) {
			Author actualAuthor = authorIterator.next();
			actualAuthorSize = theMap.get(actualAuthor).size();
			theMap.get(actualAuthor).add(book);
			
			if (theMap.get(actualAuthor).size() > actualAuthorSize) {
				addedAuthors++;
			}
		}
		
		return addedAuthors;
	}

	@Override
	public SortedSet<Author> findAuthors(BookTag tag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<Book> findBooks(String authorName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<Book> findBooksInCommon(String name1, String name2) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
