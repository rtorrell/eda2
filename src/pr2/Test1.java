package pr2;

import static utilsProva.UtilsProva.*;
import java.util.*;

public class Test1 {

	public static void main (String [] args) {
		/* test data creation */
		
		BookTag [] sortedTags = {
				new BookTag("AAAA", 23, "ZZ"),
				new BookTag("AAAA", 28, "ZZ"),
				new BookTag("AAAA", 28, "JJ"),
				new BookTag("AAAA", 28, "BA"),
				new BookTag("AAAK", 23, "ZZ"),
				new BookTag("AAAK", 28, "ZZ"),
				new BookTag("AAAK", 28, "JJ"),
				new BookTag("AAAK", 28, "BA"),
				new BookTag("DAAA", 21, "BB"),
				new BookTag("DAAA", 22, "CC"),
				new BookTag("DAAA", 23, "JJ"),
				new BookTag("DAAA", 24, "ZZ"),
				new BookTag("TAAA", 99, "RR")
		};
		
		
		
		String [] titles = {
				"monster-sized lists", // 0
				"exposed attributes of a shy collection", // 1
				"Eden Garden of data iterators", // 2
				"arraylists in action", // 3
				"a vector with two arrays", // 4
				"two arrays and a list with a vector", // 5
				"linkedlists store it better", // 6
				"a house of full lists", // 7
				"glamourous collections", // 8
				"after-school hours of a collection", // 9
				"Iterate me after class!", // 10
				"ArrayLists shows their attributes", // 11
				"fully-equipped iterators" // 12
		};
		
		int [] years = {
				1996,
				1996,
				2000, // 2
				2000,
				2000,
				2005, // 5
				2005,
				2010, // 7
				2011, // 8
				2012, // 9
				2018, // 10
				2018,
				2019 // 12
		};
		
		Author [] authors = {
			new Author("Mary Doe"),
			new Author("Natalie Figgs"),
			new Author("Pamela Hat"),
			new Author("Carlos Brown"),
			new Author("John Smith")
		};
		
		// shuffle(authors);
		
		Book [] books;
		books = new Book[sortedTags.length];
		
		for (int i=0; i<sortedTags.length; i++) {
			books[i] = new Book(sortedTags[i], years[i], titles[i]);
		}
		
		// shuffle(books);
		
		List [] fullLists = {
				Arrays.asList(books[0], books[1], books[2], books[3], books[4]),
				Arrays.asList(books[4], books[5], books[6], books[7], books[8]),
				Arrays.asList(books[7], books[8], books[9], books[10], books[11]),
				Arrays.asList(books[4], books[7], books[12]),
				Arrays.asList(),
		};
		
		List [] initialLists = {
				Arrays.asList(books[0], books[1], books[2], books[3]),
				Arrays.asList(books[5], books[6]),
				Arrays.asList(books[9], books[10], books[11]),
				Arrays.asList(books[12]),
				Arrays.asList(),
		};
		
		
		List [] authorsPerBook = {
				Arrays.asList(authors[0]),
				Arrays.asList(authors[0]),
				Arrays.asList(authors[0]),
				Arrays.asList(authors[0]),
				Arrays.asList(authors[0], authors[1], authors[3]), // for books[4]
				Arrays.asList(authors[1]),
				Arrays.asList(authors[1]),
				Arrays.asList(authors[1], authors[2], authors[3]), // for books[7]
				Arrays.asList(authors[1], authors[2]),			   // for books[8]
				Arrays.asList(authors[2]),
				Arrays.asList(authors[2]),
				Arrays.asList(authors[2]),
				Arrays.asList(authors[3]),
				Arrays.asList(authors[3]),
		};
		
		
		
		
		/* test data creation ends here */
				
		iniciar ("test 01 de MapAuthorshipTracker");
		MapAuthorshipTracker mau = new MapAuthorshipTracker();
		
		provar ("addBooksToAuthor (1)");
		try {
			for (int i=0; i<authors.length; i++ ) {
				if (mau.addBooksToAuthor(authors[i], initialLists[i])!=initialLists[i].size()) {
					notificarError("addBooksToAuthor resultat incorrecte", SORTIR);
				}
			}
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("addBooksToAuthor (1) cap comportament inesperat");
		
		provar ("addBooksToAuthor (2)");
		try {
			for (int i=0; i<authors.length; i++ ) {
				if (mau.addBooksToAuthor(authors[i], initialLists[i])!=0) {
					notificarError("addBooksToAuthor resultat incorrecte", SORTIR);
				}
			}
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("addBooksToAuthor (2) cap comportament inesperat");
		
		//---
		
		provar("addAuthorsToBook (1)");
		try {
			if (mau.addAuthorsToBook(authorsPerBook[4], books[4]) !=3)
				notificarError("addAuthorsToBook resultat incorrecte",SORTIR);
			System.out.print(".");
			if (mau.addAuthorsToBook(authorsPerBook[7], books[7]) !=3)
				notificarError("addAuthorsToBook resultat incorrecte",SORTIR);
			System.out.print(".");
			if (mau.addAuthorsToBook(authorsPerBook[8], books[8]) !=2)
				notificarError("addAuthorsToBook resultat incorrecte",SORTIR);
			System.out.print(".");
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("addAuthorsToBook (1) cap comportament inesperat");
		
		provar("addAuthorsToBook (2)");
		try {
			if (mau.addAuthorsToBook(authorsPerBook[4], books[4]) !=0)
				notificarError("addAuthorsToBook resultat incorrecte",SORTIR);
			System.out.print(".");
			if (mau.addAuthorsToBook(authorsPerBook[7], books[7]) !=0)
				notificarError("addAuthorsToBook resultat incorrecte",SORTIR);
			System.out.print(".");
			if (mau.addAuthorsToBook(authorsPerBook[8], books[8]) !=0)
				notificarError("addAuthorsToBook resultat incorrecte",SORTIR);
			System.out.print(".");
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("addAuthorsToBook (2) cap comportament inesperat");
		
		//-----------
		
		provar("findAuthors llibres coneguts");
		try {
			for (int i=0; i<books.length; i++) {
				List<Author> result =  new LinkedList<Author>(mau.findAuthors(books[i].getTag().clone()));
				if (!doubleInclusion(result, authorsPerBook[i]))
					notificarError("findAuthors continut incorrecte",SORTIR);
				if (!sortedByName(result))
					notificarError("findAuthors ordenació incorrecta",SORTIR);
			}
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("findAuthors llibres coneguts comportament correcte");
		
		provar("findAuthors llibres desconeguts");
		try {
			if (mau.findAuthors(new BookTag("XXXX", 79, "XX")).size()!=0)
				notificarError("findAuthors llibre desconegut resultat incorrecte",SORTIR);
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("findAuthors llibres desconeguts comportament correcte");
		
		//-----------------
		
		provar("findBooks autors coneguts");
		try {
			for (int i=0; i<authors.length; i++) {
				List<Book> result = new LinkedList<Book>(mau.findBooks(new String(authors[i].getName())));
				if (!doubleInclusion(result, fullLists[i]))
					notificarError("findBooks contingut incorrecte",SORTIR);
				if (!sortedByTitle(result))
					notificarError("findBooks ordenació incorrecta",SORTIR);
			}
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("findBooks autors coneguts comportament correcte");
		
		provar("findBooks autor desconegut");
		try {
			if (mau.findBooks("SAVISTRUBEL").size()!=0)
				notificarError("findBooks autor desconegut resultat incorrecte",SORTIR);
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("findBooks autor desconegut comportament correcte");
		
		//----------
		
		provar("findBooksInCommon autors coneguts");
		try {
			for (int i=0; i<authors.length; i++) {
				for (int j=i+1; j<authors.length; j++) {
					List<Book> expected = new ArrayList<Book>(fullLists[i]);
					expected.retainAll(fullLists[j]);
					List<Book> result = new ArrayList<Book>(mau.findBooksInCommon(new String(authors[i].getName()), new String(authors[j].getName())));
					if (!doubleInclusion(result, expected))
						notificarError("findBooksInCommon contingut incorrecte",SORTIR);
					if (!sortedNatural(result)) {
						for (Book b: result) System.out.println(b);
						notificarError("findBooksInCommon ordenació incorrecta",SORTIR);
					}
				}
			}
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("findBooksInCommon autors coneguts correcte");
		
		provar("findBooksInCommon autors desconeguts");
		try {
			for (int i=0; i<authors.length; i++) {
				if (mau.findBooksInCommon(new String (authors[i].getName()), "SAVISTRUBEL").size()!=0)
					notificarError("findBooksInCommon resultat incorrecte autor(2) desconegut",SORTIR);
				if (mau.findBooksInCommon("SAVISTRUBEL",new String (authors[i].getName())).size()!=0)
					notificarError("findBooksInCommon resultat incorrecte autor(1) desconegut",SORTIR);
			}
			if (mau.findBooksInCommon("LOPEDEPEGA", "SAVISTRUBEL").size()!=0)
				notificarError("findBooksInCommon resultat incorrecte autor(1 i 2) desconegut",SORTIR);
		}
		catch(Exception e) {
			notificarExcepcio(e, SORTIR);
		}
		informar("findBooksInCommon autors desconeguts comportament correcte");
		
		finalitzar();
	}
	
	private static boolean doubleInclusion (List<?> a, List<?> b) {
		for (Object o : a) 
			if (!b.contains(o)) {
				return false;
			}
		for (Object o : b) 
			if (!a.contains(o)) {
				return false;
			}
		return true;
	}
	
	private static boolean sortedByName (List<Author> list) {
		if (list.size()<=1) return true;
		String current = list.get(0).getName();
		for (int i=1; i<list.size(); i++) {
			if (!(current.compareTo(list.get(i).getName())<=0))
				return false;
			current = list.get(i).getName();
		}
		return true;
	}
	
	private static boolean sortedByTitle (List<Book> list) {
		if (list.size()<=1) return true;
		String current = list.get(0).getTitle();
		for (int i=1; i<list.size(); i++) {
			if (!(current.compareTo(list.get(i).getTitle())<=0))
				return false;
			current = list.get(i).getTitle();
		}
		return true;
	}
	
	private static boolean sortedNatural (List<Book> list) {
		if (list.size()<=1) return true;
		Book current = list.get(0);
		for (int i=1; i<list.size(); i++) {
			if (current.compareTo(list.get(i))>0)
				return false;
			current = list.get(i);
		}
		return true;
	}
	
}
